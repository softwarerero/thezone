See at one glance the current local time of your team members. Speficy at what time they usually start and end their day and see if they currently likely online or not. Add members to team and filter them by team.

This project is build on [Svelte](https://github.com/sveltejs) and [Sapper](https://github.com/sveltejs/sapper) just to learn them. Data is persistet only in the browser's localStorage, so if you don't have it you have no luck. You can export and import you members as JSON.

The project is hosted as a Gitlab static page under https://softwarerero.gitlab.io/thezone/.

# Run dev environment
npm run dev

# Build and deploy
npm run export
cp -R __sapper__/export/thezone/* public
<!-- sapper export --basepath thezone -->
git push

# Ideas
- Integrate weather: https://darksky.net/dev/docs
- Do some visualiztion a la spacetime.am