import sirv from 'sirv';
import polka from 'polka';
import compression from 'compression';
import * as sapper from '@sapper/server';
import Helpers from "./lib/helpers.js"

global.TZ = Helpers
global.TZ.Members = {
    all: () => [],
    getTeams: () => [],
}

const BASEPATH = 'thezone'
// process.env.PORT = 4000
const { PORT, NODE_ENV } = process.env;
// const { NODE_ENV } = process.env;
// const PORT = 4000
console.log(`PORT: ${PORT}, BASEPATH: ${BASEPATH}`)
const dev = NODE_ENV === 'development';

polka() // You can also use Express
	.use(
        `/${BASEPATH}`,
		compression({ threshold: 0 }),
		sirv('static', { dev }),
		sapper.middleware()
	)
	.listen(PORT, err => {
		if (err) console.log('error', err);
	});

export { sapper };
