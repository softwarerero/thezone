export default {
    members: (() => {
        if (!window) return []
        // if (!window.localStorage.getItem('members')) {
        //     const memberSeedData = [
        //         {team: "test", id: "1", name: 'Stefan', startTime: '07:00', endTime: '13:15', timeZone: 'PYT'},
        //         {team: "test", id: "3", name: 'Alex', startTime: '09:00', endTime: '18:00', timeZone: 'PST'},
        //     ]
        //     window.localStorage.setItem('members', JSON.stringify(memberSeedData))
        // }
        return JSON.parse(window.localStorage.getItem('members')) || []
    })(),

    all: (team) => {
        let members = TZ.Members.members
        if (team) {
            members = TZ.Members.members.filter(member => member.team === team)
        }
        return members
            .map(member => {
                member.localTime = TZ.localTime(member)
                member.offset = TZ.Timezones.getForAbbr(member.timeZone).offset                
                return member
            })
            .sort((a, b) => a.offset - b.offset)
    },

    saveAll: () => {
        window.localStorage.setItem('members', JSON.stringify(TZ.Members.members))
        return TZ.Members.members
    },
    
    save: (member, team = 'test') => {
        const id = member.id || TZ.uuidv4()
        const index = TZ.Members.members.findIndex((member => member.id === id))
        if (index > -1) {
            TZ.Members.members[index] = member
        } else {
            member.id = id
            member.team = member.team || team
            TZ.Members.members.push(member)
        }
        TZ.Members.saveAll()
        return TZ.Members.members
    },

    remove: (id) => {
        const index = TZ.Members.members.findIndex((member => member.id === id))
        if (index > -1) {
            TZ.Members.members.splice(index, 1);
            TZ.Members.saveAll()
            return TZ.Members.members
        }
    },

    getTeams: (id) => {
        return [... new Set(TZ.Members.members.map(m => m.team))]
    },
}
