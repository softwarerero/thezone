import { onDestroy } from 'svelte'
import dayjs from 'dayjs'
import dayjsPluginUTC from 'dayjs-plugin-utc'
dayjs.extend(dayjsPluginUTC)

export default {
    appName: 'The Zone',
    uuidv4() {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8)
            return v.toString(16)
        })
    },
    getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++) {
          color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    },
    formatTime: (d = new Date()) => dayjs(d).format('HH:mm'),
    localTime: (member) => {
        const timezone = TZ.Timezones.getForAbbr(member.timeZone)
        return dayjs().utc().add(timezone.offset, 'hours').format('HH:mm')
    },
    isWorking(member) {
        const timezone = TZ.Timezones.getForAbbr(member.timeZone)
        const localTime = +dayjs().utc().add(timezone.offset, 'hours').format('HHmm')
        const startTime = +member.startTime.replace(':', '')
        const endTime = +member.endTime.replace(':', '')
        return startTime < localTime && endTime > localTime
    },
    onInterval(callback, milliseconds) {
        const interval = setInterval(callback, milliseconds)    
        onDestroy(() => clearInterval(interval))
    },
}
