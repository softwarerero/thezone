import * as sapper from '@sapper/app'
import Helpers from "./lib/helpers.js"
import Members from "./lib/members.db.js"
import Timezones from "./lib/timezones.js"

window.TZ = Helpers
window.TZ.Members = Members
window.TZ.Timezones = Timezones

sapper.start({
	target: document.querySelector('#sapper')
});
